package com.fatalcubez.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.fatalcubez.game.GdxGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = GdxGame.SCREEN_WIDTH;
		config.height = GdxGame.SCREEN_HEIGHT;
		new LwjglApplication(new GdxGame(), config);
	}
}

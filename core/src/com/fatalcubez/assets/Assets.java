package com.fatalcubez.assets;

public enum Assets {

	BACKGROUND(0, 0, 144, 256),
	FOREGROUND(146, 0, 154, 56),
	BIRD_1(264, 64, 17, 12),
	BIRD_2(264, 90, 17, 12),
	BIRD_3(223, 124, 17, 12),
	PIPE_TOP(302, 0, 26, 135),
	PIPE_BOTTOM(330, 0, 26, 121),
	PIPE_EXTENSION(302, 0, 26, 108);

	private int x;
	private int y;
	private int width;
	private int height;
	
	private Assets(int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
}

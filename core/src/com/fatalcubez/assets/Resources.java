package com.fatalcubez.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ArrayMap;

public class Resources {

	private static Texture spritesheet = new Texture(Gdx.files.internal("flappy_sprites.png"));
	private static ArrayMap<Assets, TextureRegion> regionMap = new ArrayMap<Assets, TextureRegion>();
	
	static {
		spritesheet.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		// Load in all the sprites
		for(Assets asset : Assets.values()) {
			regionMap.put(asset, new TextureRegion(spritesheet, asset.getX(), asset.getY(), asset.getWidth(), asset.getHeight()));
		}
	}
	
	public static TextureRegion getRegion(Assets asset) {
		return regionMap.get(asset);
	}
	
	public static void dispose() {
		spritesheet.dispose();
		regionMap = null;
	}
	
	private Resources () {
	}
	
}

package com.fatalcubez.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.fatalcubez.assets.Assets;
import com.fatalcubez.assets.Resources;

public class Bird {

	private static Animation<TextureRegion> bird;
	private ShaderProgram colorShader;
	private float x;
	private float y;
	private float dy = 0.0f;
	private float rotation = 0.0f;
	private float angularSpeed = 120.0f;
	private float elapsed = 0.0f;
	private Color speciesColor = Color.PINK;
	
	static {
		ShaderProgram.pedantic = false;
		Array<TextureRegion> frames = new Array<TextureRegion>(Array.with(
				Resources.getRegion(Assets.BIRD_1),
				Resources.getRegion(Assets.BIRD_2),
				Resources.getRegion(Assets.BIRD_3)));
		bird = new Animation<TextureRegion>(0.15f, frames, PlayMode.LOOP);
	}
	
	public Bird(float x, float y) {
		this.x = x;
		this.y = y;

		colorShader = new ShaderProgram(Gdx.files.internal("bird.vert"), Gdx.files.internal("bird.frag"));
		if(!colorShader.isCompiled()) {
			throw new RuntimeException(colorShader.getLog());
		}
		colorShader.begin();
		colorShader.setUniformf("replaceColor", speciesColor.r, speciesColor.g, speciesColor.b, 1.0f);
		colorShader.end();
	}
	
	public void render(SpriteBatch batch) {
		batch.setShader(colorShader);
		TextureRegion birdRegion = bird.getKeyFrame(elapsed);
		batch.draw(birdRegion, x, y, birdRegion.getRegionWidth() * 0.5f, birdRegion.getRegionHeight() * 0.5f, birdRegion.getRegionWidth(), birdRegion.getRegionHeight(), 1.0f, 1.0f, rotation);
		batch.setShader(null);
	}
	
	public void update(float delta) {
		elapsed += delta;
		dy += GdxGame.GRAVITY * delta;
		y += dy * delta;
		rotation -= angularSpeed * delta;
	}
	
	public void flap() {
		dy = 150.0f;
		rotation = 45.0f;
	}
	
	public Rectangle getHitBox() {
		return new Rectangle(x, y, bird.getKeyFrame(0).getRegionWidth(), bird.getKeyFrame(0).getRegionHeight());
	}
	
	public float getDy() {
		return dy;
	}
	
	public void setDy(float dy) {
		this.dy = dy;
	}
	
	public float getX() {
		return x;
	}
	
	public void setX(float x) {
		this.x = x;
	}
	
	public float getY() {
		return y;
	}
	
	public void setY(float y) {
		this.y = y;
	}
	
	public float getRotation() {
		return rotation;
	}
	
	public void setRotation(float rotation) {
		this.rotation = rotation;
	}

	public float getAngularSpeed() {
		return angularSpeed;
	}
	
	public void setElapsed(float elapsed) {
		this.elapsed = elapsed;
	}
	
	public void addTime(float delta) {
		this.elapsed += delta;
	}
	
	public void setSpeciesColor(Color speciesColor) {
		this.speciesColor = speciesColor;
	}
	
	public Color getSpeciesColor() {
		return speciesColor;
	}
	
}

package com.fatalcubez.game;

import org.ejml.simple.SimpleMatrix;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;
import com.fatalcubez.ai.ConnectionGene;
import com.fatalcubez.ai.Genome;
import com.fatalcubez.ai.GenomeGraph;
import com.fatalcubez.ai.LogMonitor;
import com.fatalcubez.ai.Neat;
import com.fatalcubez.ai.NeuralNetwork;
import com.fatalcubez.ai.Node.NodeType;
import com.fatalcubez.ai.NodeManager;
import com.fatalcubez.ai.xor.XORFitnessEvaluator;
import com.fatalcubez.ai.xor.XORGenomeFactory;
import com.fatalcubez.assets.Assets;
import com.fatalcubez.assets.Resources;

public class GdxGame extends ApplicationAdapter {
	// Game Vars
	public final static int SCALE = 3;
	public final static int GAME_WIDTH = 144;
	public final static int GAME_HEIGHT = 256;
	public final static int SCREEN_WIDTH = GAME_WIDTH * SCALE;
	public final static int SCREEN_HEIGHT = GAME_HEIGHT * SCALE;
	public final static float GRAVITY = -640.0f;
	public final static float PIPE_SPEED = 45.0f;
	
	// Rendering
	private SpriteBatch batch;
	private OrthographicCamera gameCamera;
	private OrthographicCamera drawCamera;
	private FrameBuffer frameBuffer;

	// Background
	private TextureRegion background;
	private TextureRegion foreground;
	private float foregroundX = 0.0f;
	private float foregroundY = -20.0f;
	
	// Bird
	private Bird bird;
	private boolean waiting = true;
	
	// Pipes
	private Pipe[] pipes;
	private float pipeDistance = GAME_WIDTH * 0.65f;
	
	@Override
	public void create () {
		SimpleMatrix input = new SimpleMatrix(2, 1);
		input.set(0, 0, 1);
		input.set(1, 0, 1);

		NodeManager nodeManager = new NodeManager(2, 1);

		int bias = 0;
		int x1 = 1;
		int x2 = 2;
		int output = 3;
		
		Genome xorGenome = new Genome(nodeManager, 5.0);
		xorGenome.addConnection(output, bias);
		xorGenome.addConnection(output, x1);
		xorGenome.addConnection(output, x2);
		
		int or = xorGenome.createNodeInBetween(output, bias);
		int nand = xorGenome.createNodeInBetween(output, x1);
		
		// bias and x1
		xorGenome.enableGene(bias, output);
		xorGenome.addConnection(bias, nand);
		xorGenome.addConnection(x1, or);

		// x2
		xorGenome.disbleGene(x2, output);
		xorGenome.addConnection(x2, or);
		xorGenome.addConnection(x2, nand);
		
		// Set Weights
		xorGenome.setWeight(bias, output, -30.0);
		xorGenome.setWeight(bias, or, -10.0);
		xorGenome.setWeight(bias, nand, 30.0);
		xorGenome.setWeight(x1, or, 20.0);
		xorGenome.setWeight(x1, nand, -20.0);
		xorGenome.setWeight(x2, or, 20.0);
		xorGenome.setWeight(x2, nand, -20.0);
		xorGenome.setWeight(or, output, 20.0);
		xorGenome.setWeight(nand, output, 20.0);
		
		XORFitnessEvaluator eval = new XORFitnessEvaluator();
		double fit = eval.calculatePopulationFitness(Array.with(xorGenome)).first();
		System.out.println(fit);
		
//		System.out.println(xorGenome);
//		System.out.println(xorGenome.feedForward(input));
		
		Neat neat = new Neat(new XORGenomeFactory(), new XORFitnessEvaluator(), 15, new LogMonitor());
		Genome winner = neat.evolve(2, 1);
		GenomeGraph graph = new GenomeGraph(winner, "what up");
		graph.display();
		
		System.out.println(winner);
		
		SimpleMatrix inputs00 = new SimpleMatrix(2, 1);
		inputs00.set(0, 0, 0);
		inputs00.set(1, 0, 0);
		
		SimpleMatrix inputs10 = new SimpleMatrix(2, 1);
		inputs10.set(0, 0, 1);
		inputs10.set(1, 0, 0);
		
		SimpleMatrix inputs01 = new SimpleMatrix(2, 1);
		inputs01.set(0, 0, 0);
		inputs01.set(1, 0, 1);
		
		SimpleMatrix inputs11 = new SimpleMatrix(2, 1);
		inputs11.set(0, 0, 1);
		inputs11.set(1, 0, 1);
		
		System.out.println("Winner (0, 0): " + winner.feedForward(inputs00));
		System.out.println("Winner (0, 1): " + winner.feedForward(inputs01));
		System.out.println("Winner (1, 0): " + winner.feedForward(inputs10));
		System.out.println("Winner (1, 1): " + winner.feedForward(inputs11));

		
//		Genome xorGenome = champions.get(0);
		NeuralNetwork xor = new NeuralNetwork(2, new int[] { 2 }, 1);
		
		xor.setWeights(0, 0, -10, 20, 20);
		xor.setWeights(0, 1, 30, -20, -20);
		xor.setWeights(1, 0, -30, 20, 20);
		
//		System.out.println(xor.feedForward(input));
		
		NeuralNetwork negate = new NeuralNetwork(1, 1);
		negate.setWeights(0, 0, 10, -20);
		
		input = new SimpleMatrix(1, 1);
		input.set(1);
//		System.out.println(negate.feedForward(input));
		
		batch = new SpriteBatch();
		background = Resources.getRegion(Assets.BACKGROUND);
		foreground = Resources.getRegion(Assets.FOREGROUND);
		bird = new Bird(GAME_WIDTH * 0.5f - 20, GAME_HEIGHT * 0.5f);
		
		pipes = new Pipe[(int)(GAME_WIDTH / pipeDistance) + 1];
		for(int i = 0; i < pipes.length; i++) {
			Pipe pipe = new Pipe(getRandomHeight());
			pipe.setX(GAME_WIDTH + i * pipeDistance);
			pipes[i] = pipe;
		}
		
		gameCamera = new OrthographicCamera();
		gameCamera.setToOrtho(false, GAME_WIDTH, GAME_HEIGHT);
		drawCamera = new OrthographicCamera();
		drawCamera.setToOrtho(false, SCREEN_WIDTH, SCREEN_HEIGHT);
		
		frameBuffer = new FrameBuffer(Pixmap.Format.RGBA8888, SCREEN_WIDTH, SCREEN_HEIGHT, false);
		frameBuffer.getColorBufferTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
	}

	@Override
	public void render () {
		update(Gdx.graphics.getDeltaTime());
		gameCamera.update();

		frameBuffer.begin();
		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		batch.begin();
		batch.setProjectionMatrix(gameCamera.combined);
		batch.draw(background, 0, 0);
		for(Pipe pipe : pipes) {
			pipe.render(batch);
		}
		batch.draw(foreground, foregroundX, foregroundY);
		batch.draw(foreground, foregroundX + foreground.getRegionWidth(), foregroundY);
		bird.render(batch);
		batch.end();
		
		frameBuffer.end();
		
		drawCamera.update();
		batch.begin();
		batch.setProjectionMatrix(drawCamera.combined);
		batch.draw(
				frameBuffer.getColorBufferTexture(),
				0, 
				frameBuffer.getHeight(),
				frameBuffer.getWidth(), 
				-frameBuffer.getHeight()
		);
		batch.end();
	}
	
	public void update(float delta) {		
		handleInput();
		if(waiting) {
			bird.addTime(delta);
			return;
		}
		
		foregroundX -= PIPE_SPEED * delta;
		if(foregroundX <= -foreground.getRegionWidth()) foregroundX = 0;
		
		bird.update(delta);
		
		for(Pipe pipe : pipes) {
			pipe.update(delta);
			if(pipe.offScreen()) {
				int numPipes = pipes.length;
				pipe.setX(pipe.getX() + numPipes * pipeDistance);
				pipe.setY(getRandomHeight());
			}
		}
		
		checkCollision();
	}
	
	private void checkCollision() {
		Rectangle birdRect = bird.getHitBox();
		if(birdRect.y < foregroundY + foreground.getRegionHeight()) {
			reset();
			return;
		}
		
		boolean dead = false;
		for(Pipe pipe : pipes) {
			if(pipe.hittingBird(birdRect)) {
				dead = true;
				break;
			}
		}
		if(dead) {
			reset();
		}
		
	}
	
	private void reset() {
		for(int i = 0; i < pipes.length; i++) {
			pipes[i].setX(GAME_WIDTH + i * pipeDistance);
			pipes[i].setY(getRandomHeight());
		}
		bird.setY(GAME_HEIGHT * 0.5f);
		bird.setDy(0.0f);
		bird.setRotation(0.0f);
		bird.setElapsed(0.0f);
		waiting = true;
	}
	
	private float getRandomHeight() {
		return MathUtils.random(GAME_HEIGHT * 0.3f, GAME_HEIGHT * 0.8f);
	}
	
	private void handleInput() {
		if(Gdx.input.isKeyJustPressed(Keys.SPACE)) {
			if(waiting) waiting = false;
			bird.flap();
		}
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		Resources.dispose();
	}
}

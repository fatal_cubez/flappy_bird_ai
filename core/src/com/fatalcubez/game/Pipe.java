package com.fatalcubez.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.fatalcubez.assets.Assets;
import com.fatalcubez.assets.Resources;

public class Pipe {

	// Render
	private static TextureRegion top = Resources.getRegion(Assets.PIPE_TOP);
	private static TextureRegion bottom = Resources.getRegion(Assets.PIPE_BOTTOM);
	private static TextureRegion extension = Resources.getRegion(Assets.PIPE_EXTENSION);
	private float gapSize = 50.0f;
	private ShapeRenderer sr;
	private boolean debugMode = false;

	// Positioning
	private float x = 0.0f;
	private float yCenter; // y pos of center of pipe
	private Rectangle topRect;
	private Rectangle bottomRect;

	public Pipe(float yCenter) {
		this.yCenter = yCenter;
		topRect = new Rectangle();
		bottomRect = new Rectangle();
		updateHitBoxes();
		sr = new ShapeRenderer();
	}

	public void update(float delta) {
		x -= GdxGame.PIPE_SPEED * delta;
		topRect.x = x;
		bottomRect.x = x;
	}

	public void render(SpriteBatch batch) {
		// Top Pipe
		batch.draw(top, x, getTopY());
		if (GdxGame.GAME_HEIGHT - yCenter > top.getRegionHeight()) {
			batch.draw(extension, x, getTopY() + top.getRegionHeight());
		}

		// Bottom Pipe
		batch.draw(bottom, x, getBottomY());
		if (yCenter - bottom.getRegionHeight() > 0) {
			batch.draw(extension, x, getBottomY() - extension.getRegionHeight());
		}
		batch.end();

		// Debug
		if (debugMode) {
			sr.setProjectionMatrix(batch.getProjectionMatrix());
			sr.begin(ShapeType.Line);
			sr.setColor(Color.RED);
			sr.rect(topRect.x, topRect.y, topRect.width, topRect.height);
			sr.rect(bottomRect.x, bottomRect.y, bottomRect.width, bottomRect.height);
			sr.end();
		}

		batch.begin();
	}

	private void updateHitBoxes() {
		topRect.set(x, getTopY(), top.getRegionWidth(), GdxGame.GAME_HEIGHT - getTopY());
		bottomRect.set(x, 0, bottom.getRegionWidth(), yCenter - gapSize * 0.5f);
	}

	public boolean hittingBird(Rectangle bird) {
		return topRect.overlaps(bird) || bottomRect.overlaps(bird);
	}

	public void setX(float x) {
		this.x = x;
		updateHitBoxes();
	}

	public float getX() {
		return x;
	}

	public float getWidth() {
		return top.getRegionWidth();
	}

	public boolean offScreen() {
		return x + getWidth() < 0;
	}

	public void setY(float yCenter) {
		this.yCenter = yCenter;
		updateHitBoxes();
	}

	/** Returns the bottom left corner of the top pipe's texture region when rendering */
	public float getTopY() {
		return yCenter + gapSize * 0.5f;
	}

	/** Returns the bottom left corner of the bottom pipe's texture region when rendering */
	public float getBottomY() {
		return yCenter - gapSize * 0.5f - bottom.getRegionHeight();
	}

}

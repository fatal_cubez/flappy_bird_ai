package com.fatalcubez.ai;

import com.badlogic.gdx.utils.Array;

public class Simulation {

	private int currentGeneration = 0;
	private Array<Generation> generations;
	
	public Simulation() {
		generations = new Array<Generation>();
	}
	
	public void addGeneration(Generation generation) {
		generations.add(generation);
	}
	
	public Generation createNewGeneration() {
		Generation gen = new Generation(currentGeneration++);
		generations.add(gen);
		return gen;
	}
	
	public Generation getGenerationContaining(Genome genome, boolean asChild) {
		for(int i = generations.size - 1; i >= 0; i--) {
			Generation gen = generations.get(i);
			if(asChild) {
				if(gen.containsChild(genome) || gen.isChampion(genome)) return gen;
			} else {
				if(gen.containsInPopulation(genome)) return gen;
			}
		}
		return null;
	}
	
	public Generation getGeneration(int genNum) {
		if(genNum < 0 || genNum >= generations.size) return null;
		return generations.get(genNum);
	}
	
	public Genome getGenome(int id) {
		for(Generation generation : generations) {
			for(Genome gen : generation.getPopulation()) {
				if(gen.getID() == id) return gen;
			}
		}
		return null;
	}
}

package com.fatalcubez.ai;

public enum GeneType {

	MATCHING,
	DISJOINT,
	EXCESS
	
}

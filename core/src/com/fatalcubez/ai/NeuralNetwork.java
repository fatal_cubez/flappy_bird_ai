package com.fatalcubez.ai;

import org.ejml.equation.Equation;
import org.ejml.simple.SimpleMatrix;

public class NeuralNetwork {

	private int numInputs; // includes bias unit
	private int[] hiddenLayers;
	private int numOutputs;
	private SimpleMatrix[] weightMatrices;
	
	public NeuralNetwork(int numInputs, int numOutputs){
		this(numInputs, new int[]{}, numOutputs);
	}
	
	public NeuralNetwork(int numInputs, int[] hiddenLayers, int numOutputs) {
		this.numInputs = numInputs + 1;
		this.hiddenLayers = hiddenLayers;
		this.numOutputs = numOutputs;
		weightMatrices = new SimpleMatrix[1 + hiddenLayers.length];
		
		for(int i = 0; i < weightMatrices.length; i++) {
			int inputs = 0;
			int outputs = 0;
			if(i == 0) {
				inputs = this.numInputs;
			} else {
				inputs = hiddenLayers[i - 1] + 1;
			}
			if(i == weightMatrices.length - 1) {
				outputs = numOutputs;
			} else {
				outputs = hiddenLayers[i];
			}
			SimpleMatrix theta = new SimpleMatrix(inputs, outputs);
			weightMatrices[i] = theta;
		}
	}
	
	public SimpleMatrix feedForward(SimpleMatrix input) {
		if(!input.isVector() || input.numRows() + 1 != numInputs) {
			throw new IllegalArgumentException("Input must be a 1x" + numInputs + " column vector.");
		}
		
		SimpleMatrix x = new SimpleMatrix(input.numRows() + 1, 1);
		x.insertIntoThis(1, 0, input);
		x.set(0, 0, 1.0f);

		Equation eq = new Equation();

		for(int i = 0; i < weightMatrices.length; i++) {
			SimpleMatrix w1 = weightMatrices[i];
			eq.alias(w1, "w1", x, "x");
			eq.process("M = w1'*x");
			SimpleMatrix output = sigmoid(SimpleMatrix.wrap(eq.lookupMatrix("M")));
			if(i == weightMatrices.length - 1) return output;
			
			x = new SimpleMatrix(output.numRows() + 1, 1);
			x.insertIntoThis(1, 0, output);
			x.set(0, 0, 1.0f);
		}
		return null;
	}
	
	public void setWeights(int inputLayer, int activationIndex, double... weights) {
		SimpleMatrix w = weightMatrices[inputLayer];
		if(weights.length != w.numRows()) throw new IllegalArgumentException("Length of weights array must equal columns of weight matrix.");
		w.setColumn(activationIndex, 0, weights);
	}
	
	private SimpleMatrix sigmoid(SimpleMatrix vec) {
		//       1
		// -------------
		//  1   +   e^-z
		Equation eq = new Equation();
		eq.alias(vec, "z");
		eq.process("M = 1.0 /  (1.0 + exp(-z))");
		return SimpleMatrix.wrap(eq.lookupMatrix("M"));
	}
	
	public int getNumInputs() {
		return numInputs;
	}
	
	public int getNumOutputs() {
		return numOutputs;
	}
	
	public int[] getHiddenLayers() {
		return hiddenLayers;
	}
	
}

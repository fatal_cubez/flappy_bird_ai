package com.fatalcubez.ai.xor;

import org.ejml.simple.SimpleMatrix;

import com.badlogic.gdx.utils.Array;
import com.fatalcubez.ai.FitnessEvaluator;
import com.fatalcubez.ai.Genome;

public class XORFitnessEvaluator implements FitnessEvaluator{

	@Override
	public Array<Double> calculatePopulationFitness(Array<Genome> population) {
		Array<Double> results = new Array<Double>();
		
		SimpleMatrix inputs00 = new SimpleMatrix(2, 1);
		inputs00.set(0, 0, 0);
		inputs00.set(1, 0, 0);
		double output00 = 0.0;
		
		SimpleMatrix inputs10 = new SimpleMatrix(2, 1);
		inputs10.set(0, 0, 1);
		inputs10.set(1, 0, 0);
		double output10 = 1.0;
		
		SimpleMatrix inputs01 = new SimpleMatrix(2, 1);
		inputs01.set(0, 0, 0);
		inputs01.set(1, 0, 1);
		double output01 = 1.0;
		
		SimpleMatrix inputs11 = new SimpleMatrix(2, 1);
		inputs11.set(0, 0, 1);
		inputs11.set(1, 0, 1);
		double output11 = 0.0;
		
		for(Genome genome : population) {
			double error = 0.0;
			SimpleMatrix out = genome.feedForward(inputs00);
			error += Math.abs(out.get(0) - output00);
			
			out = genome.feedForward(inputs10);
			error += Math.abs(out.get(0) - output10);
			
			out = genome.feedForward(inputs01);
			error += Math.abs(out.get(0) - output01);
			
			out = genome.feedForward(inputs11);
			error += Math.abs(out.get(0) - output11);

			double fitness = 4.0 - error;
			fitness *= fitness;
			results.add(fitness);

			if(Math.abs(out.get(0) - output11) > 0.5 && fitness + 0.1 > 9.0){
				System.out.println("OR Function");
			}
			if(Math.abs(genome.feedForward(inputs00).get(0) - output00) > 0.5 && fitness + 0.1 > 9.0){
				System.out.println("NAND Function");
			}
		}
		return results;
	}

	
	
}

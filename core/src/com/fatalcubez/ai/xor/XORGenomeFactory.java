package com.fatalcubez.ai.xor;

import com.fatalcubez.ai.Genome;
import com.fatalcubez.ai.GenomeFactory;
import com.fatalcubez.ai.NodeManager;

public class XORGenomeFactory implements GenomeFactory{

	@Override
	public Genome getRandomGenome(NodeManager nodeManager, double randomWeightRange) {
		int bias = 0;
		int x1 = 1;
		int x2 = 2;
		int out = 3;
		
		Genome genome = new Genome(nodeManager, randomWeightRange);
		genome.addConnection(bias, out);
		genome.addConnection(x1, out);
		genome.addConnection(x2, out);
		return genome;
	}

	
	
}

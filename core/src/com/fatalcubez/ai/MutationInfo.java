package com.fatalcubez.ai;

public class MutationInfo {

	private double weightChange;
	private ConnectionGene connectionMade;
	private Node nodeAdded;
	private Node in;
	private Node out;
	
	public double getWeightChange() {
		return weightChange;
	}
	
	public void setWeightChange(double weightChange) {
		this.weightChange = weightChange;
	}
	
	public double getAbsoluteWeightChange() {
		return Math.abs(weightChange);
	}
	
	public ConnectionGene getConnectionMade() {
		return connectionMade;
	}
	
	public void setConnectionMade(ConnectionGene connectionMade) {
		this.connectionMade = connectionMade;
	}
	
	public boolean wasConnectionMade() {
		return connectionMade != null;
	}

	public Node getNodeAdded() {
		return nodeAdded;
	}

	public void setNodeAdded(Node nodeAdded) {
		this.nodeAdded = nodeAdded;
	}

	public Node getIn() {
		return in;
	}

	public void setIn(Node in) {
		this.in = in;
	}

	public Node getOut() {
		return out;
	}

	public void setOut(Node out) {
		this.out = out;
	}
	
	public boolean wasNodeAdded() {
		return nodeAdded != null;
	}
	
}

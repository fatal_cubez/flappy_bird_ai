package com.fatalcubez.ai;

import com.badlogic.gdx.utils.Array;

public interface FitnessEvaluator {

	public Array<Double> calculatePopulationFitness(Array<Genome> population);
	
}

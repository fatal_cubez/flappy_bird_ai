package com.fatalcubez.ai;

import static com.fatalcubez.ai.Utils.round;

import java.util.Random;

import com.badlogic.gdx.utils.Array;

public class RouletteWheel<T> {

	private Array<T> items;
	private Array<Double> values;
	private Array<Double> intervals;
	private double total;
	
	public RouletteWheel(Array<T> items, Array<Double> values) {
		if(items.size != values.size) throw new IllegalArgumentException("Arrays must be of equal size.");
		this.items = items;
		this.values = values;
		intervals = new Array<Double>(items.size);
		
		total = calcTotal(values);
		createIntervals();
	}
	
	public T spinWheel(Random random) {
		double rand = random.nextDouble();
		for(int i = 0; i < intervals.size; i++) {
			if(intervals.get(i) > rand) {
				return items.get(i);
			}
		}
		throw new RuntimeException("Roulette wheel couldn't find an item.");
	}
	
	private double calcTotal(Array<Double> values) {
		double ret = 0;
		for(Double value : values) {
			ret += value;
		}
		return ret;
	}
	
	private void createIntervals() {
		intervals.add(values.first() / total);
		for(int i = 1; i < values.size; i++) {
			intervals.add(intervals.get(i-1) + values.get(i) / total);
		}
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for(int i = 0 ; i < intervals.size; i++) {
			double begin = 0.0;
			if(i != 0) {
				begin = intervals.get(i-1);
			}
			double end = intervals.get(i);
			begin = round(begin, 3);
			end = round(end, 3);
			sb.append(items.get(i) + ": " + begin + " - " + end + "\n");
		}
		return sb.toString();
	}
	
}

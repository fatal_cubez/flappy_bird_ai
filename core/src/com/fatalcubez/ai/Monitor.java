package com.fatalcubez.ai;

public interface Monitor {

	public void onGenerationChange(int gen, int populationSize);
	public void onMutate(int numConnectionsCreated, int numNodesAdded, double averageWeightChange);
	public void onSpeciation(int numSpecies, int singleGenomeSpecies);
	
}

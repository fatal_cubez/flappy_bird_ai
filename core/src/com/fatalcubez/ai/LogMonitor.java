package com.fatalcubez.ai;

public class LogMonitor implements Monitor{

	private int generation;
	
	@Override
	public void onMutate(int numConnectionsCreated, int numNodesAdded, double averageWeightChange) {
		if(true)return;
		log("Mutation Overview\n-------------------");
		log(numConnectionsCreated + " connections created.");
		log(numNodesAdded + " nodes added.");
		log(Utils.round(averageWeightChange, 3) + " average weight change.\n");
	}

	@Override
	public void onSpeciation(int numSpecies, int singleGenomeSpecies) {
		if(true)return;
		log("Speciation Overview\n-------------------");
		log(numSpecies + " total species.");
		log(singleGenomeSpecies + " species with one member.\n");
	}

	@Override
	public void onGenerationChange(int gen, int populationSize) {
		if(true)return;
		generation = gen;
		log("Generation Overview\n-------------------");
		log(populationSize + " total members in population.\n");
	}

	private void log(String message) {
		System.out.println("Gen " + generation + ": " + message);
	}
}

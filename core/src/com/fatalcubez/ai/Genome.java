package com.fatalcubez.ai;

import org.ejml.simple.SimpleMatrix;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.fatalcubez.ai.Node.NodeType;

public class Genome {

	private ArrayMap<Integer, ConnectionGene> innovMap; // innov # to connection gene
	private Array<ConnectionGene> connectionGenes;
	private ArrayMap<Node, Array<Node>> inputNodeMap;
	private NodeManager nodeManager;
	private int speciesID;
	private double fitness;
	private double adjustedFitness;
	private double randomWeightRange; // +/-
	private int myID;
	private static int globalID;
	
	// Used for picking random nodes
	private ObjectSet<Node> nodeSet;
	private Array<Node> nodes;
	private ArrayMap<Integer, ObjectSet<Integer>> connectedNodeMap;
	
	public Genome(NodeManager nodeManager, double randomWeightRange) {
		innovMap = new ArrayMap<Integer, ConnectionGene>();
		connectionGenes = new Array<ConnectionGene>();
		inputNodeMap = new ArrayMap<Node, Array<Node>>();
		nodeSet = new ObjectSet<Node>();
		nodes = new Array<Node>();
		connectedNodeMap = new ArrayMap<Integer, ObjectSet<Integer>>();
		this.nodeManager = nodeManager;
		this.randomWeightRange = randomWeightRange;
		myID = globalID++;
	}
	
	/**
	 * Creates a copy of the passed in genome
	 * @param genome
	 */
//	public Genome(Genome genome) {
//		this.nodeManager = genome.nodeManager;
//		this.randomWeightRange = genome.randomWeightRange;
//		innovMap = new ArrayMap<Integer, ConnectionGene>(genome.innovMap);
//		connectionGenes = new Array<ConnectionGene>(genome.connectionGenes);
//		inputNodeMap = new ArrayMap<Node, Array<Node>>(genome.inputNodeMap);
//		nodeSet = new ObjectSet<Node>(genome.nodeSet);
//		nodes = new Array<Node>(genome.nodes);
//		connectedNodeMap = new ArrayMap<Integer, ObjectSet<Integer>>(genome.connectedNodeMap);
//		speciesID = genome.speciesID;
//		myID = globalID++;
//	}
	
	public NodeManager getNodeManager() {
		return nodeManager;
	}
	
	public int getSpeciesID() {
		return speciesID;
	}
	
	public void setSpeciesID(int speciesID) {
		this.speciesID = speciesID;
	}
	
	public Array<ConnectionGene> getConnectionGenes() {
		return connectionGenes;
	}
	
	public ConnectionGene getGene(int innov) {
		return innovMap.get(innov);
	}
	
	public ConnectionGene addConnection(int node1, int node2, double weight, boolean split) {
		if(node1 == node2) throw new IllegalArgumentException("A node can't be connected to itself");
		if(getConnectionGene(node1, node2) != null) throw new IllegalArgumentException("A connection between " + node1 + " and " + node2 + " already exists.");
		int input = getInput(node1, node2);
		int output = input == node1 ? node2 : node1;
		
		ConnectionGene connectionGene = new ConnectionGene(nodeManager.getNode(input), nodeManager.getNode(output),
				weight,
				nodeManager.getInnovationNumber(node1, node2));
		return addConnection(connectionGene, split);
	}
	
	/**
	 * Uses a BFS to determine which node would be the input and which one would be the output
	 * @param node1
	 * @param node2
	 * @return
	 */
	private int getInput(int node1, int node2) {
		Node n1 = nodeManager.getNode(node1);
		Node n2 = nodeManager.getNode(node2);
		if(n2.getNodeType() == NodeType.INPUT || n1.getNodeType() == NodeType.OUTPUT) return node2;
		return node1;
//		ObjectSet<Integer> visited = new ObjectSet<Integer>();
//		Queue<Integer> queue = new Queue<Integer>();
//		for(Node node : nodeManager.getInputNodes()) {
//			queue.addLast(node.getId());
//			visited.add(node.getId());
//		}
//		
//		while(queue.size > 0) {
//			Integer first = queue.removeFirst();
//			
//			for(Integer i : connectedNodeMap.get(first)) {
//				if(visited.contains(i)) continue;
//				if(i == node1) return node1;
//				if(i == node2) return node2;
//				visited.add(i);
//				queue.addLast(i);
//			}
//		}
//		return -1;
	}
	
	public ConnectionGene getConnectionGene(int input, int output) {
		if(input == output) return null;
		Pair<Integer> pair1 = new Pair<Integer>(input, output);
		for(ConnectionGene gene : connectionGenes) {
			Pair<Integer> pair2 = new Pair<Integer>(gene.getIn().getId(), gene.getOut().getId());
			if(pair1.equals(pair2)) return gene;
		}
		return null;
	}

	/**
	 * Creates a new node that splits the connection between input and output
	 * @param input
	 * @param output
	 */
	public int createNodeInBetween(int input, int output) {
		ConnectionGene gene = getConnectionGene(input, output);
		if(gene == null) throw new IllegalArgumentException("No connection between nodes " + input + " and " + output);
		Node node = nodeManager.createNode(input, output);
		setDisabled(gene, true);
		addConnectedNode(input, node.getId());
		addConnectedNode(output, node.getId());
		ConnectionGene gene1 = addConnection(gene.getIn().getId(), node.getId(), 1.0, true);
		ConnectionGene gene2 = addConnection(node.getId(), gene.getOut().getId(), gene.getWeight(), true);
		if(gene1.getOut() != gene2.getIn()) {
			throw new RuntimeException("Node split failed.");
		}
		return node.getId();
	}
	
	public ConnectionGene addConnection(int fromNode, int toNode) {
		return addConnection(fromNode, toNode, randomWeight(), false);
	}
	
	public void setWeight(int input, int output, double weight) {
		setWeight(getConnectionGene(input, output), weight);
	}
	
	public void setWeight(ConnectionGene gene, double weight) {
		if(gene == null) return;
		gene.setWeight(weight);
	}
	
	public double randomWeight() {
		return randomWeightRange * MathUtils.random.nextDouble() * 2 - randomWeightRange;
	}
	
	public void enableGene(int input, int output) {
		setDisabled(getConnectionGene(input, output), false);
	}
	
	public void disbleGene(int input, int output) {
		setDisabled(getConnectionGene(input, output), true);
	}
	
	public void setDisabled(ConnectionGene gene, boolean disabled) {
		// IF the gene is being enabled, check for cycles
		if(!disabled) {
			gene.setDisabled(false);
			boolean createdCycle = createsCycle(gene);
			gene.setDisabled(createdCycle);
		} else {
			gene.setDisabled(true);
		}
	}
	
	public ConnectionGene addConnection(ConnectionGene connectionGene) {
		return addConnection(connectionGene, false);
	}
	
	public ConnectionGene addConnection(ConnectionGene connectionGene, boolean split) {
		// Check to see if connection creates cycle
		if (!split && createsCycle(connectionGene)) {
			return null;
		} 
		connectionGenes.add(connectionGene);
		innovMap.put(connectionGene.getInnovationNumber(), connectionGene);
		addInputNode(connectionGene.getOut(), connectionGene.getIn());
		
		int size = nodeSet.size;
		nodeSet.add(connectionGene.getIn());
		if(nodeSet.size > size) nodes.add(connectionGene.getIn());
		
		size = nodeSet.size;
		nodeSet.add(connectionGene.getOut());
		if(nodeSet.size > size) nodes.add(connectionGene.getOut());
		
		addConnectedNode(connectionGene.getIn().getId(), connectionGene.getOut().getId());
		
		if(!split) {
			for(Integer n1 : connectedNodeMap.keys()) {
				for(Integer n2 : connectedNodeMap.get(n1)) {
					if(getConnectionGene(n1, n2) == null) throw new RuntimeException("Connected node map is out of sync with connection genes.");
				}
			}
		}
		
		return connectionGene;
	}
	
	private boolean createsCycle(ConnectionGene gene) {
		Node in = gene.getIn();
		Node out = gene.getOut();
		
		// Check if you can go from out to in
		return checkCycleRecursive(in, out);
	}
	
	private boolean checkCycleRecursive(Node start, Node goal) {
		if(start.getNodeType() == NodeType.INPUT) return false;
		if(start.equals(goal)) return true;
		if(inputNodeMap.get(start) == null) {
			return false;
		}
		for(Node input : inputNodeMap.get(start)) {
			ConnectionGene gene = getConnectionGene(input.getId(), start.getId());
			if(gene.isDisabled()) continue;
			if(checkCycleRecursive(input, goal)) {
				return true;
			}
		}
		return false;
	}
	
	public ConnectionGene makeRandomConnection() {
		if(allConnected()) return null;
		
		Array<Pair<Node>> nodePairs = new Array<Pair<Node>>();
		for(Node from : nodes) {
			if(from.getNodeType() == NodeType.OUTPUT) continue;
			if(!hasOpenConnections(from)) continue;
			for(int i = 0; i < nodes.size; i++) {
				Node to = nodes.get(i);
				if(from.equals(to) || to.getNodeType() == NodeType.INPUT) continue;
				if(connectedNodeMap.get(to.getId()).contains(from.getId())) continue;
				nodePairs.add(new Pair<Node>(from, to));
			}
		}
		
		if(nodePairs.size == 0) return null;
		Pair<Node> node = nodePairs.get(MathUtils.random(0, nodePairs.size - 1));
		ConnectionGene result = addConnection(node.getOne().getId(), node.getTwo().getId());
		if(result == null) {
			result = addConnection(node.getTwo().getId(), node.getOne().getId());
		}
		return result;
	}
	
	public Node getRandomNode() {
		return nodes.get(MathUtils.random(0, nodes.size - 1));
	}
	
	
	public boolean allConnected(){
		for(Node node : nodes) {
			if(hasOpenConnections(node)) return false;
		}
		return true;
	}
	
	public boolean hasOpenConnections(Node node) {
		Integer id = node.getId();
		if(nodeManager.getNode(id).getNodeType() == NodeType.INPUT) {
			if(connectedNodeMap.get(id).size + 1 == nodes.size - nodeManager.getNumInputs()) return false;
		}else {
			if(connectedNodeMap.get(id).size + 1 == nodes.size) return false;
		}	
		return true;
	}
	
	// Returns an array of 3 integers representing node ids. They represent inNode, outNode, and betweenNode, in that order.
	public int[] createRandomNode() {
		Array<ConnectionGene> copy = new Array<ConnectionGene>(connectionGenes);
		while(copy.size > 0) {
			int size = copy.size;
			int index = MathUtils.random(0, size-1);
			
			ConnectionGene gene = copy.get(index);
			int in = gene.getIn().getId();
			int out = gene.getOut().getId();
			
			Node inBetween = nodeManager.getNodeBetween(in, out);
			if(inBetween == null || !nodeSet.contains(inBetween)){
				int nodeID = createNodeInBetween(in, out);
				return new int[] { in, out, nodeID };
			}
			copy.removeIndex(index);
		}
		assert(false);
		return null;
	}
	
	private void addInputNode(Node node, Node input) {
		if(!inputNodeMap.containsKey(node)){
			inputNodeMap.put(node, new Array<Node>());
		}
		inputNodeMap.get(node).add(input);
	}
	
	private void addConnectedNode(int node1, int node2) {
		if(!connectedNodeMap.containsKey(node1)) {
			connectedNodeMap.put(node1, new ObjectSet<Integer>());
		}
		if(!connectedNodeMap.containsKey(node2)) {
			connectedNodeMap.put(node2, new ObjectSet<Integer>());
		}
		connectedNodeMap.get(node1).add(node2);
		connectedNodeMap.get(node2).add(node1);
	}
	
	public void setFitness(double fitness) {
		this.fitness = fitness;
	}
	
	public double getFitness() {
		return fitness;
	}
	
	public SimpleMatrix feedForward(SimpleMatrix inputs) {
		// Add the bias unit
		SimpleMatrix x = new SimpleMatrix(inputs.numRows() + 1, inputs.numCols());
		x.insertIntoThis(1, 0, inputs);
		x.set(0, 0, 1.0);
		
		SimpleMatrix outputs = new SimpleMatrix(nodeManager.getNumOutputs(), 1);
		for(int i = 0; i < nodeManager.getOutputNodes().size; i++) {
			Node output = nodeManager.getOutputNodes().get(i);
			double value = 0.0;
			value = recursiveSolve(output, x);
			outputs.set(i, 0, value);
		}
		return outputs;
	}
	
	private double recursiveSolve(Node node, SimpleMatrix inputs) {
		if(node.getNodeType() == NodeType.INPUT) {
			return inputs.get(node.getId(), 0);
		}
		
		double activation = 0.0;
		for(Node input : inputNodeMap.get(node)) {
			ConnectionGene connection = getConnectionGene(input.getId(), node.getId());
			if(connection.isDisabled()) continue;
			double value = recursiveSolve(input, inputs);
			value = value * connection.getWeight();
			activation += value;
		}
		
		// Sigmoid
		return 1.0 / (1.0 + Math.exp(-4.9 * activation));
	}
	
	public int getHighestInnovNumber(){
		int max = Integer.MIN_VALUE;
		for(Integer integer : innovMap.keys()) {
			max = Math.max(max, integer);
		}
		return max;
	}
	
	public boolean hasGene(int innov) {
		return innovMap.containsKey(innov);
	}
	
	public int numGenes() {
		return connectionGenes.size;
	}
	
	public void setAdjustedFitness(double adjustedFitness) {
		this.adjustedFitness = adjustedFitness;
	}
	
	public double getAdjustedFitness() {
		return adjustedFitness;
	}
	
	public int getID() {
		return myID;
	}
	
	@Override
	public String toString() {
//		String connections = "" + connectionGenes;
//		connections = connections.substring(1, connections.length() - 1);
//		connections = connections.replace(",", "\n\n");
		String connections = "";
		return "My ID: " + myID + "\nSpecies - " + speciesID + "\nFitness - " + Utils.round(fitness, 3) + "\n\n" + connections;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(adjustedFitness);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((connectedNodeMap == null) ? 0 : connectedNodeMap.hashCode());
		result = prime * result + ((connectionGenes == null) ? 0 : connectionGenes.hashCode());
		temp = Double.doubleToLongBits(fitness);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((innovMap == null) ? 0 : innovMap.hashCode());
		result = prime * result + ((inputNodeMap == null) ? 0 : inputNodeMap.hashCode());
		result = prime * result + myID;
		result = prime * result + ((nodeSet == null) ? 0 : nodeSet.hashCode());
		result = prime * result + ((nodes == null) ? 0 : nodes.hashCode());
		result = prime * result + speciesID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Genome other = (Genome) obj;
		if (Double.doubleToLongBits(adjustedFitness) != Double.doubleToLongBits(other.adjustedFitness)) return false;
		if (connectedNodeMap == null) {
			if (other.connectedNodeMap != null) return false;
		} else if (!connectedNodeMap.equals(other.connectedNodeMap)) return false;
		if (connectionGenes == null) {
			if (other.connectionGenes != null) return false;
		} else if (!connectionGenes.equals(other.connectionGenes)) return false;
		if (Double.doubleToLongBits(fitness) != Double.doubleToLongBits(other.fitness)) return false;
		if (innovMap == null) {
			if (other.innovMap != null) return false;
		} else if (!innovMap.equals(other.innovMap)) return false;
		if (inputNodeMap == null) {
			if (other.inputNodeMap != null) return false;
		} else if (!inputNodeMap.equals(other.inputNodeMap)) return false;
		if (myID != other.myID) return false;
		if (nodeSet == null) {
			if (other.nodeSet != null) return false;
		} else if (!nodeSet.equals(other.nodeSet)) return false;
		if (nodes == null) {
			if (other.nodes != null) return false;
		} else if (!nodes.equals(other.nodes)) return false;
		if (speciesID != other.speciesID) return false;
		return true;
	}
}

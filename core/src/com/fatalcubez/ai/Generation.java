package com.fatalcubez.ai;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;

public class Generation {

	private int generationNumber;
	private Array<Genome> population;
	private Array<Genome> champions;
	private Array<Genome> parents;
	private ArrayMap<Genome, Pair<Genome>> childToParents;
	private ArrayMap<Genome, CrossOverData> childToCross;
	private ArrayMap<Genome, MutationData> childToMutation;
	private SpeciesData speciesData;
	
	public Generation(int generationNumber) {
		this.generationNumber = generationNumber;
		population = new Array<Genome>();
		champions = new Array<Genome>();
		parents = new Array<Genome>();
		childToParents = new ArrayMap<Genome, Pair<Genome>>();
		childToCross = new ArrayMap<Genome, CrossOverData>();
		childToMutation = new ArrayMap<Genome, MutationData>();
	}
	
	public void addGenomeToPopulation(Genome genome) {
		population.add(genome);
	}
	
	public void addGenomesToPopulation(Array<? extends Genome> genomes) {
		population.addAll(genomes);
	}
	
	public void addParent(Genome genome) {
		parents.add(genome);
	}
	
	public void addParents(Array<? extends Genome> parents) {
		this.parents.addAll(parents);
	}
	
	public void addChild(Genome child, Genome parentA, Genome parentB) {
		childToParents.put(child, new Pair<Genome>(parentA, parentB));
	}
	
	public boolean hasParents(Genome genome) {
		return childToParents.containsKey(genome);
	}
	
	public Pair<Genome> getParents(Genome child){
		return childToParents.get(child);
	}
	
	public void addChampion(Genome champion) {
		champions.add(champion);
	}
	
	public void addChampions(Array<Genome> champions) {
		this.champions.addAll(champions);
	}
	
	public void addCrossOverData(Genome child, CrossOverData data) {
		childToCross.put(child, data);
	}
	
	public void addMutationData(Genome child, MutationData data) {
		childToMutation.put(child, data);
	}
	
	public void setSpeciesData(SpeciesData speciesData) {
		this.speciesData = speciesData;
	}
	
	public SpeciesData getSpeciesData() {
		return speciesData;
	}
	
	public int getGenerationNumber() {
		return generationNumber;
	}
	
	public boolean containsInPopulation(Genome genome) {
		return population.contains(genome, false);
	}
	
	// INCOMPLETE Doesn't account for champions
	public boolean containsChild(Genome genome) {
		return childToParents.containsKey(genome);
	}
	
	public boolean isChampion(Genome genome){
		return champions.contains(genome, false);
	}
	
	public Array<Genome> getPopulation() {
		return population;
	}
	
}
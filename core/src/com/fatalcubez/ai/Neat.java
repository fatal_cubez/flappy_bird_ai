package com.fatalcubez.ai;

import java.util.Comparator;
import java.util.Random;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.badlogic.gdx.utils.Sort;
import com.fatalcubez.ai.PerturbData.PerturbType;

public class Neat {

	// User Defined Functions
	private GenomeFactory genomeFactory;
	private FitnessEvaluator fitnessEval;
	private Monitor monitor;
	
	// Helpful Maps
	private Simulation simulation;
//	private ArrayMap<Integer, Generation> generationMap;
	private ArrayMap<Integer, Array<Genome>> speciesMap;
	private int speciesID = 0;
	private ArrayMap<Integer, Double> maxFitnessMap;
	private ArrayMap<Integer, Integer> stagnationMap;
	private ObjectSet<Integer> bannedSpecies;

	// Hyper Parameters
	private int populationSize;
	private double fitnessGoal;
	private double disabledGeneChance;
	private double perturbChance;
	private double perturbStepSize;
	private double perturbRandomChance;
	private double mutateConnectionChance;
	private double mutateNodeChance;
	private double randomWeightRange;
	private double championPercent;
	private double selectionThreshold;
	private double fitnessIncreaseThreshold;
	private double selectionRate;
	private int stagnationThreshold;

	// Specification Parameters
	private double c1;
	private double c2;
	private double c3;
	private double compatibilityThreshold;
	private int targetSpeciesAmount;
	private double compatMod;

	// Node
	private NodeManager nodeManager;
	private int generationNum = 0;

	public Neat(GenomeFactory genomeFactory, FitnessEvaluator fitnessEval, double fitnessGoal, Monitor monitor) {
		this.genomeFactory = genomeFactory;
		this.fitnessEval = fitnessEval;
		this.fitnessGoal = fitnessGoal;
		this.monitor = monitor;
		
//		generationMap = new ArrayMap<Integer, Generation>();
		speciesMap = new ArrayMap<Integer, Array<Genome>>();
		maxFitnessMap = new ArrayMap<Integer, Double>();
		stagnationMap = new ArrayMap<Integer, Integer>();
		bannedSpecies = new ObjectSet<Integer>();

		populationSize = 150;
		disabledGeneChance = 0.75;
		perturbChance = 0.8;
		perturbStepSize = 2.5;
		perturbRandomChance = 0.1;
		mutateConnectionChance = 0.5;
		mutateNodeChance = 0.3;
		randomWeightRange = 2.5;
		championPercent = 0.015;
		selectionThreshold = 0.5;
		fitnessIncreaseThreshold = 0.2;
		selectionRate = 0.75;
		stagnationThreshold = 15;
		
		c1 = 1.0;
		c2 = 1.0;
		c3 = 0.4;
		compatibilityThreshold = 13.0;
		targetSpeciesAmount = 10;
		compatMod = 0.2;
		
//		MathUtils.random.setSeed(0);
		simulation = new Simulation();
	}

	public int getPopulationSize() {
		return populationSize;
	}

	public void setPopulationSize(int populationSize) {
		this.populationSize = populationSize;
	}

	private Array<Genome> createInitialPopulation(){
		Array<Genome> population = new Array<Genome>();
		for(int i = 0; i < populationSize; i++) {
			Genome genome = genomeFactory.getRandomGenome(nodeManager, randomWeightRange);
			genome.setSpeciesID(0);
			addGenomeToSpecies(0, genome);
			population.add(genome);
		}
		maxFitnessMap.put(0, 0.0);
		stagnationMap.put(0, 0);
		return population;
	}
	
	public Genome evolve(int inputs, int outputs) {
		nodeManager = new NodeManager(inputs, outputs);
		
		// Create initial population
		Array<Genome> population = createInitialPopulation();
		
		boolean evolving = true;
		generationNum = 0;
		int numChampions = (int)(populationSize * championPercent);
	
		while(evolving) {
			Generation generation = simulation.createNewGeneration();
			System.out.println("\nGeneration: " + generationNum);
			monitor.onGenerationChange(generationNum, population.size);

			// Test each individual and get fitness (check for termination)
			Array<Double> fitnesses = fitnessEval.calculatePopulationFitness(population);
			int[] histogram = calcHistogram(fitnesses, 0, 15, 15);
			printHistogram(histogram);
			
			
			// Sets the fitness value in each genome, no longer need to keep population / fitness as parallel arrays
			adjustFitnesses(population, fitnesses);

			// Check if genome has evolved all the way
			for(Genome genome : population) {
				if(genome.getFitness() > fitnessGoal) {
					return genome;
				}
			}
			
			// Sort genomes in descending order based upon adjusted fitness
			Sort.instance().sort(population, new Comparator<Genome>(){
				@Override
				public int compare(Genome gen1, Genome gen2) {
					if(gen1.getAdjustedFitness() < gen2.getAdjustedFitness()) return 1;
					if(gen2.getAdjustedFitness() < gen1.getAdjustedFitness()) return -1;
					return 0;
				}
			});
			generation.addGenomesToPopulation(population);
			
			// Get list of champions
			Array<Genome> champions = new Array<Genome>();
			for(int i = 0; i < (int)(championPercent * populationSize); i++) {
				champions.add(population.get(i));
			}
			
			ArrayMap<Integer, Double> averageFitnessMap = new ArrayMap<Integer, Double>();
			ArrayMap<Integer, Integer> allottedOffspring = new ArrayMap<Integer, Integer>();
			double totalAverage = 0.0;
			for(Integer speciesID : speciesMap.keys()) {
				if(bannedSpecies.contains(speciesID)) continue;
				Array<Genome> genomes = speciesMap.get(speciesID);
				double speciesTotal = getTotalFitness(genomes);
				double average = speciesTotal / (double)genomes.size;
				totalAverage += average;
				averageFitnessMap.put(speciesID, average);
			}
			
			for(Integer speciesID : averageFitnessMap.keys()) {
				if(bannedSpecies.contains(speciesID)) continue;
				int numOffspring = (int)(averageFitnessMap.get(speciesID) / totalAverage * populationSize);
				allottedOffspring.put(speciesID, numOffspring);
			}

//			int selectionCutoff = (int)(selectionThreshold * population.size);
//			population.removeRange(selectionCutoff, population.size - 1);
			
			// Selection
//			RouletteWheel<Genome> populationWheel = new RouletteWheel<Genome>(population, getFitnesses(population));

			ArrayMap<Integer, RouletteWheel<Genome>> subPopulationWheels = new ArrayMap<Integer, RouletteWheel<Genome>>();
			for(Integer integer : allottedOffspring.keys()) {
				Array<Genome> subPopulation = speciesMap.get(integer);
				RouletteWheel<Genome> wheel = new RouletteWheel<Genome>(subPopulation, getFitnesses(subPopulation, true));
				subPopulationWheels.put(integer, wheel);
			}

			System.out.println("Best Fitness: " + champions.first().getFitness() + "");
			
			Array<Genome> parents = new Array<Genome>();
			Random random = MathUtils.random;
			int count = 0;
			for(Integer id : allottedOffspring.keys()) {
				int amount = allottedOffspring.get(id);
				for(int i = 0; i < amount; i++) {
					Genome parentA = subPopulationWheels.get(id).spinWheel(random);
					Genome parentB = subPopulationWheels.get(id).spinWheel(random);
					parents.add(parentA);
					parents.add(parentB);
				}
			}
//			for(int i = 0; i < populationSize - numChampions; i++){
//				Genome parentA = populationWheel.spinWheel(random);
//				int speciesID = parentA.getSpeciesID();
//				Genome parentB = subPopulationWheels.get(speciesID).spinWheel(random);
//				if(parentA.equals(parentB)) {
//					count++;
//				}
//				parents.add(parentA);
//				parents.add(parentB);
//			}
			generation.addParents(parents);
			System.out.println("Same Parents: " + count);

			// Cross-over
			Array<Genome> children = crossOver(parents, generation);
			
			// Mutation
			for(int i = 0; i < children.size; i++){
				Genome gen = children.get(i);
				MutationData data = mutate(gen);
				generation.addMutationData(gen, data);
			}
			
			// Add back in the champions
			children.addAll(champions);
			generation.addChampions(champions);
//			
//			
//			GenomeGraph graph = new GenomeGraph(champions.first(), "best");
//			graph.display(2000);
			
//			if(generationNum == 9) {
//				FamilyTree tree = new FamilyTree(simulation, champions.first());
//				tree.display();
//				try {
//					Thread.sleep(100000);
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//			}
//			if(generationNum > 15) {
//				XORFitnessEvaluator eval = new XORFitnessEvaluator();
//				eval.calculatePopulationFitness(Array.with(champions.first()));
//			}
//			if(generationNum == 4) {
//				Genome genome = simulation.getGenome(182);
//				GenomeGraph graph = new GenomeGraph(genome, "graph");
//				graph.display();
//				FamilyTree tree = new FamilyTree(simulation, genome);
//				tree.display();
//				try {
//					Thread.sleep(2000000);
//				} catch (Exception e) {
//				}
//			}

			// Specification
			
			// Check for stagnation
			for(Integer speciesID : speciesMap.keys()) {
				Array<Genome> subPop = speciesMap.get(speciesID);
				double maxFitness = getMaxFitness(subPop);
				double prevMaxFitness = maxFitnessMap.get(speciesID);
				
				// If the max fitness has increased by more than the threshold, reset the stagnation count
				if(maxFitness - fitnessIncreaseThreshold > prevMaxFitness) {
					stagnationMap.put(speciesID, 0);
					maxFitnessMap.put(speciesID, maxFitness);
				} 
				// Otherwise increment the stagnation map
				else {
					stagnationMap.put(speciesID, stagnationMap.get(speciesID) + 1);
				}
				
				// If the species had been stagnate for too long, ban it from reproducing
				if(stagnationMap.get(speciesID) >= stagnationThreshold) {
					bannedSpecies.add(speciesID);
				}
			}
			System.out.println("Banned: " + bannedSpecies);
			
			ArrayMap<Integer, Genome> previousSpecies = new ArrayMap<Integer, Genome>();
			for(Integer key : speciesMap.keys()) {
				previousSpecies.put(key, speciesMap.get(key).first());
			}
			// Clear out old species
			speciesMap.clear();

			// Look at incoming population and split into species
			for(Genome child : children) {
				boolean foundSpecies = false;
				for(Integer speciesId : previousSpecies.keys()) {
					double compatibility = getCompatibility(child, previousSpecies.get(speciesId));
					if(compatibility < compatibilityThreshold) {
						addGenomeToSpecies(speciesId, child);
						foundSpecies = true;
						break;
					}
				}
				if(!foundSpecies) {
					int speciesID = createNewSpecies();
					addGenomeToSpecies(speciesID, child);
					stagnationMap.put(speciesID, 0);
					maxFitnessMap.put(speciesID, 0.0);
				}
			}
			// Check for species with only one member
			int singleMemberSpecies = 0;
			for(Integer speciesId : speciesMap.keys()){
				Array<Genome> genomes = speciesMap.get(speciesId);
				if(genomes.size == 1) {
					Genome copy = new Genome(nodeManager, randomWeightRange);
					copy.setSpeciesID(speciesId);
					for(ConnectionGene gene : genomes.first().getConnectionGenes()){
						copy.addConnection(new ConnectionGene(gene));
					}
					// INCOMPLETE Doesn't update generation properly
					mutate(copy);
					genomes.add(copy);
					children.add(copy);
					singleMemberSpecies++;
				}
			}
			
			// Auto-Regulation of Compatibility Threshold
			if(speciesMap.size > targetSpeciesAmount) {
				compatibilityThreshold += compatMod;
			} else if(speciesMap.size < targetSpeciesAmount) {
				compatibilityThreshold -= compatMod;
			}
			if(compatibilityThreshold < compatMod) {
				compatibilityThreshold = compatMod;
			}
			
			monitor.onSpeciation(speciesMap.size, singleMemberSpecies);
			System.out.println("Total Species: " + speciesMap.size);
			population = children;
			
			generationNum++;
		}
		return null;
	}
	
	private Array<Genome> crossOver(Array<Genome> parents, Generation generation){
		Array<Genome> children = new Array<Genome>();
		for(int i = 0; i < parents.size; i+=2) {
			CrossOverData data = new CrossOverData();
			Genome parentA = parents.get(i);
			Genome parentB = parents.get(i+1);
			data.setParentA(parentA);
			data.setParentB(parentB);
			
			int highestInnovNumber = Math.max(parentA.getHighestInnovNumber(), parentB.getHighestInnovNumber());
			boolean equal = Math.abs(parentA.getFitness() - parentB.getFitness()) < 0.00001;
			boolean aFitter = parentA.getFitness() >= parentB.getFitness();
			data.setEqual(equal);
			
			Genome child = new Genome(nodeManager, randomWeightRange);
			for(int j = 0; j <= highestInnovNumber; j++) {
				// If both parents have the gene, choose gene randomly for child
				boolean aHas = parentA.hasGene(j);
				boolean bHas = parentB.hasGene(j);
				
				// If neither parent has gene with innovation number 'j', skip
				if(!aHas && !bHas) continue;
				
				ConnectionGene gene = null;
				if(aHas && bHas) {
					Genome randParent = MathUtils.random.nextDouble() < 0.5 ? parentA : parentB;
					gene = new ConnectionGene(randParent.getGene(j));
					child.addConnection(gene);
					data.setGeneParent(gene, randParent);
					data.setGeneType(gene, GeneType.MATCHING);
				}
				else {
					// Must be disjoint or excess gene
					gene = new ConnectionGene(aHas ? parentA.getGene(j) : parentB.getGene(j));
					if(equal || (aHas == aFitter)) {
						child.addConnection(gene);
						data.setGeneParent(gene, aHas ? parentA : parentB);
						
						GeneType type = GeneType.DISJOINT;
						if((aHas && j > parentB.getHighestInnovNumber()) || (bHas && j > parentA.getHighestInnovNumber())) {
							type = GeneType.EXCESS;
						}
						data.setGeneType(gene, type);
					} 
				}
				
				if((aHas && parentA.getGene(j).isDisabled()) || (bHas && parentB.getGene(j).isDisabled())) {
					boolean disabled = MathUtils.random.nextDouble() < disabledGeneChance;
					child.setDisabled(gene, disabled);
					data.setGeneDisabled(gene, disabled);
				}
			}
			children.add(child);
			generation.addChild(child, parentA, parentB);
			generation.addCrossOverData(child, data);
		}
		return children;
	}

	private MutationData mutate(Genome genome){
		MutationData data = new MutationData();
		data.setGenome(genome);
		for(int j = 0; j < genome.getConnectionGenes().size; j++) {
			ConnectionGene gene = genome.getConnectionGenes().get(j);

			// Perturb
			double perturb = MathUtils.random.nextDouble();

			if(perturb < perturbChance) {
				PerturbData perturbData = new PerturbData();
				perturb = MathUtils.random.nextDouble();
				
				// Randomize Weight
				double weightBefore = gene.getWeight();
				perturbData.setWeightBefore(weightBefore);
				if(perturb < perturbRandomChance) {
					gene.setWeight(genome.randomWeight());
					perturbData.setType(PerturbType.RANDOM);
				} else {
					gene.setWeight(gene.getWeight() + 2 * MathUtils.random.nextDouble() * perturbStepSize - perturbStepSize);
					perturbData.setType(PerturbType.ADJUST);
				}
				double weightAfter = gene.getWeight();
				perturbData.setWeightAfter(weightAfter);
			}
		}

		double rand = MathUtils.random.nextDouble();

		// Add a new connection
		if(rand < mutateConnectionChance) {
			ConnectionGene gene = genome.makeRandomConnection();
			data.setConnectionMade(gene);
		}
		rand = MathUtils.random.nextDouble();

		// Add a new node
		if(rand < mutateNodeChance) {
//			ConnectionGene randomGene = genome.getRandomConnectionGene();
//			int nodeId = genome.createNodeInBetween(randomGene.getIn().getId(), randomGene.getOut().getId());
			int[] result = genome.createRandomNode();
			
			int in = result[0];
			int out = result[1];
			int nodeId = result[2];
			
			ConnectionGene gene1 = genome.getConnectionGene(in, nodeId);
			ConnectionGene gene2 = genome.getConnectionGene(nodeId, out);
			data.setNodeAdded(gene1, gene2);
		}
		return data;
	}
	
	private Array<Double> getFitnesses(Array<Genome> genomes, boolean adjusted){
		Array<Double> ret = new Array<Double>();
		for(Genome gen : genomes) {
			ret.add(adjusted ? gen.getAdjustedFitness() : gen.getFitness());
		}
		return ret;
	}

	private void addGenomeToSpecies(int speciesID, Genome genome) {
		if(!speciesMap.containsKey(speciesID)) {
			speciesMap.put(speciesID, new Array<Genome>());
		}
		speciesMap.get(speciesID).add(genome);
		genome.setSpeciesID(speciesID);
	}

	private void adjustFitnesses(Array<Genome> genomes, Array<Double> fitnesses){
		for(int i = 0; i < fitnesses.size; i++) {
			double fitness = fitnesses.get(i);
			Genome gen = genomes.get(i);
			gen.setFitness(fitness);
			fitness /= speciesMap.get(gen.getSpeciesID()).size;
			fitnesses.set(i, fitness);
			gen.setAdjustedFitness(fitness);
		}
	}

	private int createNewSpecies() {
		speciesID++;
		return speciesID;
	}
	
	private double getCompatibility(Genome gen1, Genome gen2) {
		int highestInnovationNumber = Math.max(gen1.getHighestInnovNumber(), gen2.getHighestInnovNumber());

		double weightDifferenceTotal = 0.0;
		int matchingGenes = 0;
		int excess = 0;
		int disjoint = 0;

		for(int i = 0; i <= highestInnovationNumber; i++) {
			boolean aHas = gen1.hasGene(i);
			boolean bHas = gen2.hasGene(i);

			if(!aHas && !bHas) continue;

			if(aHas && bHas) {
				matchingGenes++;
				double difference = Math.abs(gen1.getGene(i).getWeight() - gen2.getGene(i).getWeight());
				weightDifferenceTotal += difference;
			} else {
				// If one has the gene and not the other, and the innovation number is outside the range of the other
				// then the gene must be excess
				if((aHas && i > gen2.getHighestInnovNumber()) || 
						(bHas && i > gen1.getHighestInnovNumber())) {
					excess++;
				} else {
					disjoint++;
				}
			}
		}

		double w = weightDifferenceTotal / (double) matchingGenes;
		double n = Math.max(gen1.numGenes(), gen2.numGenes());
		return (c1 * excess) / n + (c2 * disjoint) / n + c3 * w;
	}
	
	public static int[] calcHistogram(Array<Double> data, double min, double max, int numBins) {
		final int[] result = new int[numBins];
		final double binSize = (max - min)/numBins;

		for (double d : data) {
			int bin = (int) ((d - min) / binSize);
			if (bin < 0) { /* this data is smaller than min */ }
			else if (bin >= numBins) { /* this data point is bigger than max */ }
			else {
				result[bin] += 1;
			}
		}
		return result;
	}
	
	public void printHistogram(int[] histogram) {
		for(int i = 0; i < histogram.length; i++){
			int lower = i;
			int higher = i+1;
			System.out.println(lower + "-" + higher +": " + histogram[i]);
		}
	}
	
	public double getTotalFitness(Array<Genome> genomes) {
		double total = 0.0;
		for(Genome gen : genomes) total += gen.getFitness();
		return total;
	}
	
	public double getMaxFitness(Array<Genome> genomes) {
		double max = 0.0;
		for(Genome genome : genomes) {
			max = Math.max(max, genome.getFitness());
		}
		return max;
	}
}

package com.fatalcubez.ai;

public class Node {

	private NodeType nodeType;
	private int id;
	
	public Node(NodeType nodeType, int id) {
		this.nodeType = nodeType;
		this.id = id;
	}
	
	public NodeType getNodeType() {
		return nodeType;
	}
	
	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return nodeType + "[" + id + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((nodeType == null) ? 0 : nodeType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Node other = (Node) obj;
		if (id != other.id) return false;
		if (nodeType != other.nodeType) return false;
		return true;
	}

	public enum NodeType {
		INPUT,
		HIDDEN,
		OUTPUT;
		
		public String toString() {
			return name().charAt(0) + name().toLowerCase().substring(1);
		}
	}
}

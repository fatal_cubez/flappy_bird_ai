package com.fatalcubez.ai;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.graphicGraph.GraphicElement;
import org.graphstream.ui.view.Camera;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerListener;
import org.graphstream.ui.view.ViewerPipe;

public class FamilyTree {

	private Simulation sim;
	private TreeNode root;
	private Generation startingGeneration;
	
	public FamilyTree(Simulation sim, Genome genome) {
		this.sim = sim;
		initTree(genome);
	}
	
	private void initTree(Genome genome) {
		root = new TreeNode();
		root.setValue(genome);
		startingGeneration = sim.getGenerationContaining(root.getValue(), true);
	
		if(startingGeneration.isChampion(genome)) {
			root.setChampion(true);
		}
		root.setParents(recursiveCreate(root.getValue(), startingGeneration.getGenerationNumber()));
	}
	
	public void display() {
		Graph graph = new SingleGraph("Family Tree");
		graph.setStrict(false);
		graph.setAutoCreate(true);
		
		recursiveEdgeAdd(graph, root, 0, 0, 0);
		
		for(Edge edge : graph.getEdgeSet()) {
			Object[] pos = edge.getAttribute("position");
			org.graphstream.graph.Node n0 = edge.getNode0();
			org.graphstream.graph.Node n1 = edge.getNode1();
			n0.setAttribute("xy", pos[0], pos[1]);
			n1.setAttribute("xy", pos[2], pos[3]);
		}
		
		final Viewer view = graph.display();
		view.disableAutoLayout();
		
		final Camera cam = view.getDefaultView().getCamera();
		final Graph copyGraph = graph;
		view.getDefaultView().addMouseWheelListener(new MouseWheelListener() {
			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				double val = e.getPreciseWheelRotation();
				if(val < 0) {
					cam.setViewPercent(cam.getViewPercent() / 1.2);
				} else {
					cam.setViewPercent(cam.getViewPercent() * 1.2);
				}
			}
		});
		view.getDefaultView().addMouseMotionListener(new MouseMotionListener() {
			@Override
			public void mouseMoved(MouseEvent e) {
			}
			
			@Override
			public void mouseDragged(MouseEvent e) {
			}
		});
		view.getDefaultView().addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				e.getPoint();
				GraphicElement element = view.getDefaultView().findNodeOrSpriteAt(e.getPoint().x, e.getPoint().y);
				if(element == null) return;
				String id = element.getId();
				org.graphstream.graph.Node n = copyGraph.getNode(id);
				GenomeGraph graph = new GenomeGraph(n.getAttribute("genome", Genome.class), "Test");
				graph.display();
			}
		});
	}
	
	private void recursiveEdgeAdd(Graph graph, TreeNode root, int x, int y, int layer) {
		String l1 = "[" + layer + "]";
		String l2 = "[" + (layer + 1) + "]";
		if(root.isChampion() && root.getParentA() != null) {
			String n1 = root.getValue().getID() + l1;
			String n2 = root.getParentA().getValue().getID() + l2;
			System.out.println("Creating champ edge from " + n1 + " to " + n2);
			graph.addEdge(n1 + "-" + n2, n1, n2, true);
			Edge edge = graph.getEdge(n1 + "-" + n2);
			edge.addAttribute("position", x, y, x, y-1);
			edge.addAttribute("ui.label", "champ");
			edge.getNode0().addAttribute("ui.label", n1);
			edge.getNode1().addAttribute("ui.label", n2);
			edge.getNode0().addAttribute("genome", root.getValue());
			edge.getNode1().addAttribute("genome", root.getParentA().getValue());
			recursiveEdgeAdd(graph, root.getParentA(), x, y - 1, layer + 1);
		}
		else {
			if(root.getParentA() != null ) {
				String n1 = root.getValue().getID() + l1;
				String n2 = root.getParentA().getValue().getID() + l2;
				System.out.println("Creating edge from " + n1 + " to " + n2);
				graph.addEdge(n1 + "-" + n2, n1, n2, true);
				Edge edge = graph.getEdge(n1 + "-" + n2);
				edge.addAttribute("position", x, y, x - 1 , y-1);
				edge.addAttribute("ui.label", "parentA");
				edge.getNode0().addAttribute("ui.label", n1);
				edge.getNode1().addAttribute("ui.label", n2);
				edge.getNode0().addAttribute("genome", root.getValue());
				edge.getNode1().addAttribute("genome", root.getParentA().getValue());
				recursiveEdgeAdd(graph, root.getParentA(), 2 * (x - 1), y - 1, layer + 1);
			}
			if(root.getParentB() != null){
				String n1 = root.getValue().getID() + l1;
				String n2 = root.getParentB().getValue().getID() + l2;
				System.out.println("Creating edge from " + n1 + " to " + n2);
				graph.addEdge(n1 + "-" + n2, n1, n2, true);
				Edge edge = graph.getEdge(n1 + "-" + n2);
				edge.addAttribute("position", x, y, x + 1, y-1);
				edge.addAttribute("ui.label", "parentB");
				edge.getNode0().addAttribute("ui.label", n1);
				edge.getNode1().addAttribute("ui.label", n2);
				edge.getNode0().addAttribute("genome", root.getValue());
				edge.getNode1().addAttribute("genome", root.getParentB().getValue());
				recursiveEdgeAdd(graph, root.getParentB(), 2 * (x + 1), y - 1, layer + 1);
			}
		}
	}
	
	private Pair<TreeNode> recursiveCreate(Genome value, int generation){
		int prevGen = generation - 1;
		if(prevGen < 0) return null;
		
		Generation gen = sim.getGeneration(generation);
		
		if(gen.isChampion(value)) {
			TreeNode node = new TreeNode();
			node.setChampion(true);
			node.setValue(value);
			node.setParents(recursiveCreate(value, prevGen));
			return new Pair<FamilyTree.TreeNode>(node, node);
		} else {
			Pair<Genome> parents = gen.getParents(value);
			TreeNode parentA = new TreeNode();
			TreeNode parentB = new TreeNode();
			
			parentA.setValue(parents.getOne());
			parentB.setValue(parents.getTwo());
			
			parentA.setParents(recursiveCreate(parentA.getValue(), prevGen));
			parentB.setParents(recursiveCreate(parentB.getValue(), prevGen));
			return new Pair<TreeNode>(parentA, parentB);
		}
	}

	public static class TreeNode {
		private TreeNode parentA;
		private TreeNode parentB;
		private Genome value;
		private boolean champion;
		
		public TreeNode getParentA() {
			return parentA;
		}
		
		public TreeNode getParentB() {
			return parentB;
		}
		
		public Genome getValue() {
			return value;
		}
		
		public void setParentA(TreeNode parentA) {
			this.parentA = parentA;
		}
		
		public void setParentB(TreeNode parentB) {
			this.parentB = parentB;
		}
		
		public void setValue(Genome value) {
			this.value = value;
		}
		
		public void setParents(Pair<TreeNode> parentPair) {
			if(parentPair == null) return;
			parentA = parentPair.getOne();
			parentB = parentPair.getTwo();
		}

		public boolean isChampion() {
			return champion;
		}

		public void setChampion(boolean champion) {
			this.champion = champion;
		}
		
		@Override
		public String toString() {
			if(parentA == null && parentB == null) {
				return "" + value.getID();
			}
			return value.getID() + " from " + parentA.getValue().getID() + " and " + parentB.getValue().getID();
		}
	}
}

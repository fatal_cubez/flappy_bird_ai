package com.fatalcubez.ai;

public class Pair<T> {

	private T one;
	private T two;
	
	public Pair(T one, T two) {
		if(one == null || two == null) throw new IllegalArgumentException("Pair must contain two non-null values.");
		this.one = one;
		this.two = two;
	}

	public T getOne() {
		return one;
	}
	
	public T getTwo() {
		return two;
	}
	
	@Override
	public String toString() {
		return one + " - " + two;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 47;
		int piece1 = prime * result + ((one == null) ? 0 : one.hashCode());
		int piece2 = prime * result + ((two == null) ? 0 : two.hashCode());
		return piece1 + piece2;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Pair other = (Pair) obj;
		return equals(other);
	}
	
	private boolean equals(Pair other) {
		return (one.equals(other.one) && two.equals(other.two)) || (one.equals(other.two) && two.equals(other.one));
	}
	
	
	
}

package com.fatalcubez.ai;

import com.badlogic.gdx.utils.ArrayMap;

public class CrossOverData {

	private Genome parentA;
	private Genome parentB;
	private boolean equal;
	private ArrayMap<ConnectionGene, Genome> geneToParent;
	private ArrayMap<ConnectionGene, GeneType> geneToType;
	private ArrayMap<ConnectionGene, Boolean> geneToDisabled;
	
	public CrossOverData() {
		geneToParent = new ArrayMap<ConnectionGene, Genome>();
		geneToType = new ArrayMap<ConnectionGene, GeneType>();
		geneToDisabled = new ArrayMap<ConnectionGene, Boolean>();
	}

	public void setGeneParent(ConnectionGene gene, Genome parent) {
		geneToParent.put(gene, parent);
	}
	
	public void setGeneType(ConnectionGene gene, GeneType type) {
		geneToType.put(gene, type);
	}
	
	public void setGeneDisabled(ConnectionGene gene, boolean disabled) {
		geneToDisabled.put(gene, disabled);
	}
	
	public Genome getParentA() {
		return parentA;
	}

	public Genome getParentB() {
		return parentB;
	}

	public boolean isEqual() {
		return equal;
	}

	public void setParentA(Genome parentA) {
		this.parentA = parentA;
	}

	public void setParentB(Genome parentB) {
		this.parentB = parentB;
	}

	public void setEqual(boolean equal) {
		this.equal = equal;
	}

}

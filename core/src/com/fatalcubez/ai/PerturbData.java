package com.fatalcubez.ai;

public class PerturbData {

	private PerturbType type;
	private double weightBefore;
	private double weightAfter;
	
	public PerturbType getType() {
		return type;
	}

	public double getWeightBefore() {
		return weightBefore;
	}

	public double getWeightAfter() {
		return weightAfter;
	}

	public void setType(PerturbType type) {
		this.type = type;
	}

	public void setWeightBefore(double weightBefore) {
		this.weightBefore = weightBefore;
	}

	public void setWeightAfter(double weightAfter) {
		this.weightAfter = weightAfter;
	}

	public enum PerturbType {
		ADJUST,
		RANDOM
	}
}

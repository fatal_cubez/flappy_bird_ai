package com.fatalcubez.ai;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.fatalcubez.ai.Node.NodeType;

public class NodeManager {

	private int numInputs;
	private int numOutputs;
	private int id;
	private ArrayMap<Integer, Node> nodeMap; // maps node id to node
	private Array<Node> inputNodes;
	private Array<Node> outputNodes;
	private ArrayMap<Pair<Integer>, Integer> nodeIDMap; // maps pair of node ids to id of split node
	private ArrayMap<Pair<Node>, Integer> innovationMap;
	private int innovNumber;
	
	public NodeManager(int numInputs, int numOutputs){
		this.numInputs = numInputs;
		this.numOutputs = numOutputs;
		id = -1;
		nodeMap = new ArrayMap<Integer, Node>();
		inputNodes = new Array<Node>();
		outputNodes = new Array<Node>();
		nodeIDMap = new ArrayMap<Pair<Integer>, Integer>();
		innovationMap = new ArrayMap<Pair<Node>, Integer>();
		initMap();
	}
	
	private void initMap() {
		// Accounts for bias node
		for(int i = 0; i < numInputs + 1; i++){
			createNode(NodeType.INPUT);
		}
		for(int i = 0; i < numOutputs; i++){
			createNode(NodeType.OUTPUT);
		}
	}
	
	public int getNumInputs() {
		return numInputs;
	}
	
	public Array<Node> getInputNodes() {
		return inputNodes;
	}
	
	public int getNumOutputs() {
		return numOutputs;
	}
	
	public Array<Node> getOutputNodes() {
		return outputNodes;
	}
	
	public int getId() {
		return id;
	}
	
	public Node getNode(int id) {
		return nodeMap.get(id);
	}
	
	private Node createNode(NodeType type) {
		Node node = new Node(type, ++id);
		nodeMap.put(node.getId(), node);
		
		if(node.getNodeType() == NodeType.INPUT) {
			inputNodes.add(node);
		}
		
		if(node.getNodeType() == NodeType.OUTPUT) {
			outputNodes.add(node);
		}
		
		return node;
	}
	
	public Node createNode(int in, int out) {
		Pair<Integer> pair = new Pair<Integer>(in, out);
		
		// If a node exists between in and out, return that node
		if(nodeIDMap.get(pair) != null) {
			return nodeMap.get(nodeIDMap.get(pair));
		}
		
		// We need a new node
		id++;
		Node node = new Node(NodeType.HIDDEN, id);
		
		nodeIDMap.put(pair, id);
		nodeMap.put(id, node);
		
		return node;
	}
	
	public boolean nodeExistsBetween(int in, int out) {
		Pair<Integer> pair = new Pair<Integer>(in, out);
		return nodeIDMap.containsKey(pair);
	}
	
	public Node getNodeBetween(int in, int out) {
		if(!nodeExistsBetween(in, out)) return null;
		return nodeMap.get(nodeIDMap.get(new Pair<Integer>(in, out)));
	}
	
	public int getInnovationNumber(int input, int output) {
		Pair<Node> pair = new Pair<Node>(getNode(input), getNode(output));
		if(innovationMap.containsKey(pair)) {
			return innovationMap.get(pair);
		}
		// New innov number required
		innovationMap.put(pair, innovNumber++);
		return innovNumber - 1;
	}
	
}

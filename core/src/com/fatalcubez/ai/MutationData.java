package com.fatalcubez.ai;

import com.badlogic.gdx.utils.ArrayMap;

public class MutationData {

	private Genome genome;
	private ConnectionGene connectionMade;
	private ConnectionGene connectionAdded1;
	private ConnectionGene connectionAdded2;
	private ArrayMap<ConnectionGene, PerturbData> geneToPerturb;
	
	public MutationData() {
		geneToPerturb = new ArrayMap<ConnectionGene, PerturbData>();
	}
	
	public void setGenome(Genome genome) {
		this.genome = genome;
	}
	
	public Genome getGenome() {
		return genome;
	}
	
	public void setConnectionMade(ConnectionGene connectionMade) {
		this.connectionMade = connectionMade;
	}
	
	public ConnectionGene getConnectionMade() {
		return connectionMade;
	}
	
	public boolean wasConnectionMade() {
		return connectionMade != null;
	}
	
	public void setNodeAdded(ConnectionGene gene1, ConnectionGene gene2){
		connectionAdded1 = gene1;
		connectionAdded2 = gene2;
	}
	
	public ConnectionGene getConnectionAdded1() {
		return connectionAdded1;
	}
	
	public ConnectionGene getConnectionAdded2() {
		return connectionAdded2;
	}
	
	public boolean wasNodeAdded(){
		return connectionAdded1 != null && connectionAdded2 != null;
	}
	
	public void addPerturbData(ConnectionGene gene, PerturbData data) {
		geneToPerturb.put(gene, data);
	}
}

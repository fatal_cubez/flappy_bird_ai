package com.fatalcubez.ai;

public class ConnectionGene {

	private Node in;
	private Node out;
	private double weight;
	private boolean disabled = false;
	private int innovationNumber;
	
	public ConnectionGene(Node in, Node out, double weight, int innovationNumber) {
		if(in == null || out == null) throw new IllegalArgumentException("In and Out nodes must be non-null values.");
		this.in = in;
		this.out = out;
		this.weight = weight;
		this.innovationNumber = innovationNumber;
	}
	
	public ConnectionGene(ConnectionGene gene) {
		in = gene.in;
		out = gene.out;
		weight = gene.weight;
		disabled = gene.disabled;
		innovationNumber = gene.innovationNumber;
	}

	public Node getIn() {
		return in;
	}

	public Node getOut() {
		return out;
	}

	public double getWeight() {
		return weight;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public int getInnovationNumber() {
		return innovationNumber;
	}

	public void setIn(Node in) {
		this.in = in;
	}

	public void setOut(Node out) {
		this.out = out;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public void setInnovationNumber(int innovationNumber) {
		this.innovationNumber = innovationNumber;
	}

	@Override
	public String toString() {
		return in + " -> " + out + "\nWeight: " + weight + "\nDisabled: " + disabled + "\nInnov: " + innovationNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (disabled ? 1231 : 1237);
		result = prime * result + ((in == null) ? 0 : in.hashCode());
		result = prime * result + innovationNumber;
		result = prime * result + ((out == null) ? 0 : out.hashCode());
		long temp;
		temp = Double.doubleToLongBits(weight);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		ConnectionGene other = (ConnectionGene) obj;
		if (disabled != other.disabled) return false;
		if (in == null) {
			if (other.in != null) return false;
		} else if (!in.equals(other.in)) return false;
		if (innovationNumber != other.innovationNumber) return false;
		if (out == null) {
			if (other.out != null) return false;
		} else if (!out.equals(other.out)) return false;
		if (Double.doubleToLongBits(weight) != Double.doubleToLongBits(other.weight)) return false;
		return true;
	}
	
	
	
}

package com.fatalcubez.ai;

import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.Viewer.CloseFramePolicy;

import com.fatalcubez.ai.Node.NodeType;

public class GenomeGraph {

	private Genome genome;
	private Graph graph;
	
	public GenomeGraph(Genome genome, String title){
		graph = new SingleGraph(title, false, true);
		for(ConnectionGene gene : genome.getConnectionGenes()) {
			if(gene.isDisabled()) continue;
			String in = gene.getIn().getId() + "";
			String out = gene.getOut().getId() + "";
			graph.addEdge(in + "-" + out, in, out, true);
		}
		
		int inputY = 3;
		int outputY = 2;
		int hiddenY = 1;
		for(Node node : graph) {
			int id = Integer.parseInt(node.getId());
			com.fatalcubez.ai.Node n = genome.getNodeManager().getNode(id);
			node.addAttribute("ui.label", n);
			if(n.getNodeType() == NodeType.INPUT) {
				node.addAttribute("ui.class", "input");
				node.setAttribute("xy", 0, inputY--);
			}
			if(n.getNodeType() == NodeType.OUTPUT) {
				node.addAttribute("ui.class", "output");
				node.setAttribute("xy", 2, outputY++);
			}
			if(n.getNodeType() == NodeType.HIDDEN) {
				node.setAttribute("xy", 1, hiddenY++);
			}
			
		}
		for(Edge edge : graph.getEachEdge()) {
			Node node1 = edge.getNode0();
			Node node2 = edge.getNode1();
			ConnectionGene gene = genome.getConnectionGene(Integer.parseInt(node1.getId()), Integer.parseInt(node2.getId()));
			edge.setAttribute("ui.label", Utils.round(gene.getWeight(), 3));
		}
		
		String styleSheet =
		        "node.input {" +
		        "	fill-color: red;" +
		        "}" +
		        "node.output {" +
		        "   fill-color: blue;" +
		        "}";
		
		graph.addAttribute("ui.stylesheet", styleSheet);
	}
	
	public void display() {
		Viewer viewer = graph.display();
		viewer.disableAutoLayout();
		viewer.setCloseFramePolicy(CloseFramePolicy.CLOSE_VIEWER);
	}
	
	public void display(long millis) {
		display();
		try {
			Thread.sleep(millis);
		} catch (Exception e) {
		}
	}

	
}

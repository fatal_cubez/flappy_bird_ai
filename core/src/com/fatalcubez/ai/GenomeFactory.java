package com.fatalcubez.ai;


public interface GenomeFactory {

	/**
	 * Returns a random starting genome
	 * @return
	 */
	public Genome getRandomGenome(NodeManager nodeManager, double randomWeightRange);
	
}

varying vec4 v_color;
varying vec2 v_texCoord0;

uniform sampler2D u_texture;
uniform vec4 replaceColor;

vec4 colorMain;
vec4 colorShadow;
vec4 colorHighlight;

void main(){
	colorMain = vec4(0.812, 0.761, 0.173, 1.0);
	colorShadow = vec4(0.863, 0.509, 0.137, 1.0);
	colorHighlight = vec4(0.862, 0.894, 0.694, 1.0);
	vec4 color = texture2D(u_texture, v_texCoord0) * v_color;
	
	if(all(lessThan(abs(color - colorMain), vec4(0.01)))) {
		color = replaceColor;
	}
	if(all(lessThan(abs(color - colorShadow), vec4(0.01)))){
		color = vec4(replaceColor.xyz * 0.75, 1.0);
	}
	if(all(lessThan(abs(color - colorHighlight), vec4(0.01)))){
		color = vec4(replaceColor.xyz + 0.6, 1.0);
	}
	
	gl_FragColor = color;
}